// shortened means "not only was this range deleted, the underlying text was
// removed as well"

export function deleteRange(range,start,length,shortened=false) {
  var rangeEnd = range.start + range.length;
  var deleteEnd = start+length;

  // we want start and end to be the limits of the proposed range removal within
  // the range we're targeting.  They both need to be before the end and after
  // the beginning:
  var boundedStart = Math.min(rangeEnd,Math.max(range.start,start));
  var boundedEnd = Math.max(range.start,Math.min(deleteEnd,rangeEnd));

  return [
    { ...range, start: range.start, length: boundedStart - range.start },
    { ...range, start: boundedEnd - (shortened?length:0), length: rangeEnd - boundedEnd }
  ].filter(h => h.length > 0);
}

export function splitRange(range,start,length,callback) {
  var start = Math.max(0,start - range.start);
  var end = Math.min(start+length,range.start+range.length);

  return [
    { ...range, start: 0, length: start },
    callback({ ...range,start:start, length: end-start }),
    { ...range, start: end, length: range.length - end }
  ].filter(h => h.length > 0);
}

export function splitOne(span,start,length,attrs={}) {
  start -= span.offset || 0;
  var end = start+length;
  var text = span.text;

  start = Math.max(0,start);
  end = Math.max(0,Math.min(text.length,end));

  var offset = span.offset;

  return [
    { ...span ,  text: text.substring(0,start), offset: offset },
    { ...span , ...attrs, text: text.substring(start,end), offset: offset+start},
    { ...span , text: text.substring(end), offset: offset+end },
  ].filter((h) => h.text.length > 0);
}

export const split = (spans,start,length,attrs) =>
  spans.reduce((res,span) => res.concat(splitOne(span,start,length,attrs)),[])

export const isChild = (parent,node) => {
  while(node) {
    if(node == parent) return true;
    node = node.parentNode;
  }
  return false;
}

export function parents(node) {
  var nodes = [node]
  for (; node; node = node.parentNode) {
    nodes.unshift(node)
  }
  return nodes
}

// expand out
export const textOffset = (outerNode,innerNode,innerOffset) => {
  var node = innerNode;
  var offset = 0;

  if(isChild(outerNode,innerNode)) {
    while(node && node != outerNode) {
      // Reverse depth first traversal:
      if(node.firstChild) node = node.firstChild;
      else if(node.previousSibling) node = node.previousSibling;
      else node = node.parentNode && node.parentNode.previousSibling;

      if(node) {
        if(node.nodeName == '#text') {
          offset += node.textContent.length;
        }
        if(node.nodeName == 'BR') {
          offset += 1;
        }
      }
    }

    return offset+ innerOffset;
  }

  return null;
};

// does a come before b?
export const nodesInOrder = (a,b) => {
  var aParents = parents(a)
  var bParents = parents(b)

  if (aParents[0] != bParents[0]) throw new Error("text offset error: no common ancestor!");

  for (var i = 0; i < aParents.length; i++) {
    if (aParents[i] != bParents[i]) {
      var commonParent = aParents[i-1];
      // now we just need to figure out if the
      var aIndex = Array.prototype.indexOf.call(commonParent.childNodes,aParents[i]);
      var bIndex = Array.prototype.indexOf.call(commonParent.childNodes,bParents[i]);

      return a < b;
    }
  }

  throw new Error("Shouldn't get here.");
}


// return [innerNode,innerOffset]
export const reverseTextOffset = (outerNode,outerOffset) => {
  var node = outerNode;
  // length is how much text we've slurped in walking the children
  var length = 0;

  while(node && length <= outerOffset) {
    if(node.nodeName == '#text') {
      if(outerOffset <= length + node.textContent.length) {
        return [node,outerOffset-length];
      }
      length += node.textContent.length;
    }
    if(node.nodeName == 'BR') {
      length += 1;
    }


    // Depth-first traversal:
    if(node.firstChild) node = node.firstChild;
    else if(node.nextSibling) node = node.nextSibling;
    else node = node.parentNode && node.parentNode.nextSibling;
  }

  return [outerNode,0];
}

export const collapseMutations = (mutations) => {
  var ret = [];
  var last = null;
  for(var i = 0 ; i < mutations.length ; i++) {
    var m = mutations[i];

    if(!last) {
      last = m;
      ret.push(m);
    } else {
      if(last.type == m.type && m.type == "characterData" && last.target == m.target) {
        // No need to report this twice - we're comparing against the "real" value.
      } else {
        ret.push(m)
        last = m;
      }
    }
  }
  return ret;
}

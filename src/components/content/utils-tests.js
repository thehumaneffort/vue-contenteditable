require('babel-register');

var test = require('tape');
var utils = require('./utils');

test('split range',function(t) {
  console.log("Split range: ",utils.splitRange({ start: 8, length: 4, bold: true },8,2,(h) => Object.assign({},h,{bold: false })));

  t.end();
});

test('nonintersecting before',function(t) {
  var res = utils.splitOne({ text: '0123456789', offset: 80 },3,10);

  t.deepEqual(res,
    [ { text: '0123456789', offset: 80 }],
    'nonintersectingh before'
  );

  t.end();
});

test('nonintersecting after',function(t) {
  var res = utils.splitOne({ text: '0123456789', offset: 30 },40,10);

  t.deepEqual(res,[{text: '0123456789', offset: 30 }]);

  t.end();
});

test('beginning split case',function(t) {
  var res = utils.splitOne({ text: '0123456789', offset: 5}, 3,5);

  t.deepEqual(res,[{text:'012',offset: 5},
  { text: '3456789', offset: 8 }],'beginning split case');

  t.end();
});

test('end split case',function(t) {
  var res = utils.splitOne({ text: '0123456789', offset: 4}, 10, 10);

  t.deepEqual(res,[{text:'012345',offset: 4},{text:'6789', offset: 10 }],
  'end split case');

  t.end();
});

test('middle split case',function(t) {
  var res = utils.splitOne({ text: '0123456789', offset: 2 },4,2)

  t.deepEqual(res,
    [ { text: '01', offset: 2 },
    { text: '23', offset: 4 },
    { text: '456789', offset: 6 } ],
    'middle split case.'
  )

  t.end();
});

test('comprehensive',function(t) {
  var res = utils.split([
    { text: '01234', offset: 3 },
    { text: '56789', offset: 8 },
  ],6,5);

  t.deepEqual(res, [ { text: '012', offset: 3 },
  { text: '34', offset: 6 },
  { text: '567', offset: 8 },
  { text: '89', offset: 11 } ]);

  t.end();
});

test('delete from beginning',function(t) {
  var res = utils.deleteRange({ start: 5, length: 2 },4,2);

  t.deepEqual(res,[{ start: 6, length: 1 }]);

  t.end();
});

test('delete from end',function(t) {
  var res = utils.deleteRange({ start: 5, length: 2 },6,3);

  t.deepEqual(res,[{ start: 5, length: 1 }]);

  t.end();
});


test('delete all',function(t) {
  var res = utils.deleteRange({ start: 5, length: 2 },5,2);

  t.deepEqual(res,[]);

  t.end();
});


test('delete middle',function(t) {
  var res = utils.deleteRange({ start: 5, length: 3 },6,1);

  t.deepEqual(res,[{start: 5, length: 1},{start:7,length:1}]);

  t.end();
});

test('delete after end',function(t) {
  var res = utils.deleteRange({start:5,length:3},100,1);

  t.deepEqual(res,[{start: 5, length: 3}]);

  t.end();
});

test('delete before beginning',function(t) {
  var res = utils.deleteRange({start:100,length:3},5,1);

  t.deepEqual(res,[{start: 100, length: 3}]);

  t.end();
})

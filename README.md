# vue-contenteditable

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



## Musings

The goal is to simplify significantly the architectural complexity of Draft by
eliminating some unimportant pieces.  A document is represented by some data,
which is passed to a set of Vue components, selected by name (referenced
semi-abstractly in the data).  Each component corresponds to a logical
component, and can have logical children.  There is only one kind of component,
and the top-level piece is just a component that takes in an array object and
translates it into child components.  Components roughly correspond to tags, but
in general can be more complicated.  For example, an Image component corresponds
to  an <img> tag, but may presentationally chose to have other tags around it
and inside it.  Every component has a few different things that it does:

1) Translate input data into DOM.
2) Update the input data based on user interaction
  a) This might be in the form of mouse clicks on the chrome presented or
  b) In response to keypresses or other events
3) Lossily translate pasted DOM into input data

Components are registered in an order, for example:

- Paragraph
- Header
- Image
- Code Blocks
- Link
- Bold
- Italics
- Underline
- Monospace
- Strikethrough

All events are delegated based on this hierarchy.  For example, we might define
a keyboard shortcut "Cmd-B" which we intend for "Bold" to take.  When it is
received, it receives a "selection" object, which knows about what components it
has as children.  "Bold" (or, more likely, a shared ancestor of
Bold/Italics/etc.) then takes this event and issues update commands to the
underlying data, signaling that various chunks of text now have an additional
flag associated with them.  This changed data traverses back down through the
presentational components and affects the DOM.  Direct manipulation of the DOM
is illegal and punishable by developer pain.

Continuing this example, the Bold component modified the initial data to include
a bolded section:

[
{ key: <uniq>,
  type: 'Paragraph',
  text: 'This is a paragraph',
  styles: [
    { key: <uniq>, type: 'bold', range: { start: 5, length: 2 } }
  ]
}
]

Now, we have a Paragraph object, which has some text and some other data, and it
needs to figure out what to do with it.  It generates its child objects by
taking its "text" field and the rest of its data and passing it through the
chain, possibly starting below itself.  The data passes through Image, Header,
Code Blocks, and Link unchanged.  Arriving at Bold, bold splits the text into
three text blocks, each with their "original" start range offsets stored in
metadata, and returns an array of children, an unstyled text node, a Bold Vue
component with the text "is" contained therein, followed by an unstyled text
node.

If there was a subsequent Italics component, it would only be allowed to further
split the code.  Every text node that Bold returns is passed through the
remainder of the component chain (through Italics, Underline, Monospace,
Strikethrough), which continue to apply any changes they see fit along the way.

This means that certain elements can never contain other elements - for example,
it is impossible to contain a paragraph component inside of an image component
(though the image component could elect to use a <p> tag somewhere).

Let's take a concrete example around image dragging.  When an image is dragged
in from somewhere, the event is received by the top level editor component, and
passed through the list of components, picked up by the Image component.
Depending on where the image landed, it can choose to affect the underlying data
in any way the programmer deems fit.  For example, we might choose to insert a
placeholder image, with its size and positioning dependent on where the
drag-and-drop piece is hovering at the moment.  Once dropped, the Image
component emits a data change event that places the new image in the markup. How
it chooses to store the data server-side is up to the implementor of the Image
component.

In another example, we paste some rich piece of markup in the text.  Our goal is
to convert it into legal source data, and we use the same ordered list of
components to accomplish that goal.  Every component can implement a DOM->data
method for lossy conversion.  This takes 
